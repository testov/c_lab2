#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define row 10
#define len 8

int main()
{
	int IndRow, IndCol;
	char passwd[len] = {0};
	srand(time(0));
	for (IndRow=0;IndRow<row;IndRow++)
	{
		for (IndCol=0;IndCol<len;IndCol++)
		{
			switch (rand()%3)
			{
			case 0:
				passwd[IndCol] = 'A' + rand()%('Z'-'A'+1);
				break;
			case 1:
				passwd[IndCol] = 'a' + rand()%('z'-'a'+1);
				break;
			case 2:
				passwd[IndCol] = '0' + rand()%('9'-'0'+1);
				break;
			}
		}
		passwd[IndCol] = 0;
		puts(passwd);
	}
	return 0;
}
