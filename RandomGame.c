#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main()
{
	int randNum = 0, userNum = 0;
	srand(time(0));
	randNum = 1 + rand()%100;
	printf("Please enter number from 1 to 100\n");
	scanf("%d", &userNum);
	while (userNum != randNum)
	{
		if (userNum<randNum)
		{
			printf("No! You should increase value\nPlease try again\n");
			scanf("%d", &userNum);
		}
		else
		{
			printf("No! You should decrease value\nPlease try again\n");
			scanf("%d", &userNum);
		}
	}
	printf("Exactly\n");
	return 0;
}