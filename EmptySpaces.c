#include <stdio.h>
#include <string.h>

int main()
{
	char buf[80] = {0};
	int len, Ind, IndInt;
	puts("Enter a string:");
	fgets(buf,80,stdin);
	len = strlen(buf);
	for (Ind=0;Ind<len;Ind++)
	{
		if (buf[Ind]==' ' && buf[Ind-1]==' ')
		{
			for(IndInt=Ind-1; IndInt<len;IndInt++)
				buf[IndInt] = buf[IndInt+1];
			Ind--;
		}	
	}
	if (buf[0]==' ')
	{
		for(IndInt=0; IndInt<len;IndInt++)
			buf[IndInt] = buf[IndInt+1];
	}	
	if (buf[len-1]==' ')
		buf[len-1]=0;
	puts(buf);
	return 0;
}