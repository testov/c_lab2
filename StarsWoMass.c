#include <stdio.h>

int main()
{
	int row=0, numStar=0, Ind, numStarCurr, IndInt, step;
	printf("Please enter number of rows\n");
	scanf("%d", &row);
	numStar = 2*row-1;
	for (Ind=0;Ind<row;Ind++)
	{
		numStarCurr = 2*Ind + 1;
		step = (numStar - numStarCurr)/2;
		for (IndInt=0;IndInt<numStar;IndInt++)
		{
			if (IndInt>=step && IndInt<(numStar-step))
				printf("*");
			else
				printf(" ");
		}
		printf("\n");
	}
	return 0;
}