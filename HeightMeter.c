#include <stdio.h>
#include <math.h>
#include <time.h>

int main()
{
	const float g = 9.81;
	float height = 0, heightCurr = 0, time = 0, timeSquare = 0;
	int Ind;
	printf("What height will we use to launch bomb?\n");
	scanf("%f", &height);
	timeSquare = 2*height/g;
	time = pow((float)timeSquare,(float)0.5);
	for (Ind=0;Ind<time;Ind++)
	{
		heightCurr = height - g * pow(Ind, 2) / 2;
		printf("t=%d c h=%f m\n", Ind, heightCurr);
	}
	printf("BABAH\n");
	return 0;
}