#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define len 5

int main()
{
	char passwd[len]={0}, passwdRed[len]={0};
	int Ind, Count;
	srand(time(0));
	for (Ind=0;Ind<len;Ind++)
	{
		switch (rand()%3)
		{
		case 0:
			passwd[Ind]= 'a' + rand()%('z'-'a'+1);
			break;
		case 1:
			passwd[Ind]= 'A' + rand()%('Z'-'A'+1);
			break;
		case 2:
			passwd[Ind]= '0' + rand()%('9'-'0'+1);
			break;
		}
	}
	passwd[Ind] = 0;
	puts(passwd);
	printf("\n");
	
	Ind = 0;
	Count = 0;
	while (passwd[Ind])
	{
		if (passwd[Ind]>='A' && (passwd[Ind])<='Z')
		{
			passwdRed[Count] = passwd[Ind];
			Count++;
		}
		Ind++;
	}

	Ind = 0;
	while (passwd[Ind])
	{
		if (passwd[Ind]>='a' && passwd[Ind]<='z')
		{
			passwdRed[Count] = passwd[Ind];
			Count++;
		}
		Ind++;
	}

	Ind = 0;
	while (passwd[Ind])
	{
		if (passwd[Ind]>='0' && passwd[Ind]<='9')
		{
			passwdRed[Count] = passwd[Ind];
			Count++;
		}
		Ind++;
	}
	passwdRed[Ind] = 0;
	puts(passwdRed);
	printf("\n");

	return 0;
}